# Jenkins Lighthouse Test Documentation

Lighthouse is an open-source, automated tool from Google that helps us improve the quality and performance of our web applications and websites. It serves as a powerful and comprehensive auditing tool designed to assist web developers, designers, and site owners in optimizing their web pages for better user experiences.


1. `docker-compose.yml`:
   - Defines a Docker Compose configuration for running a Jenkins instance with specific settings, including port mappings, volume mounts, and network configuration.

2. `Dockerfile`:
   - Provides instructions for building a custom Jenkins Docker image. It installs necessary packages, sets up Jenkins plugins, and configures the environment for Jenkins.

3. `Jenkinsfile`:
   - Defines a Jenkins pipeline that automates the Lighthouse testing of a specified URL. It includes stages for running the Lighthouse test, reading and parsing the test result, and generating a report.

To run the code we've provided, follow these steps:

1. **Set Up Docker**: Ensure that we have Docker installed on our system. If we haven't already installed Docker, we can download and install it from the official [Docker website](https://www.docker.com/get-started).

2. **Create the Docker Compose File**:
   - Create a file named `docker-compose.yml` and copy the provided YAML configuration into it.

3. **Run Jenkins Using Docker Compose**:
   - In the terminal, navigate to the directory containing our `docker-compose.yml` file.
   - Run the following command to start Jenkins using Docker Compose:

   ```bash
   docker-compose up -d
   ```

   The `-d` flag runs the containers in detached mode, allowing them to run in the background.

4. **Access Jenkins Web Interface**:
   - After running the `docker-compose up` command, Jenkins should be running.
   - Open a web browser and access Jenkins at `http://localhost:8080`.

5. **Unlock Jenkins**:
   - During the first run, Jenkins will require us to unlock it. To obtain the unlock key, check the terminal where we ran `docker-compose up -d`. We should see a message with instructions and the unlock key.

6. **Configure Jenkins**:
   - Set up Jenkins according to our requirements, including creating jobs and configuring the build environment.

7. **Create a Jenkins Pipeline**:
   - Create a new Jenkins pipeline job and use the `Jenkinsfile` provided in our project. This pipeline automates the Lighthouse testing of a specified URL.

8. **Run the Pipeline**:
    - Build and run the Jenkins pipeline, providing the URL we want to test as a parameter when prompted.

9. **View Results**:
    - The pipeline will execute the Lighthouse test, generate a report, and provide the test results within Jenkins.

10. **Cleanup** (Optional):
    - When we're done testing, we can stop and remove the Docker containers by running:

    ```bash
    docker-compose down
    ```

